/**
 * Created by Mnill on 18.04.15.
 */

function AstraObject (x, y, type, width, height, proto) {
    this.type = type;
    this.pObj = new PObject(x, y, width, height, new Vec2(0,0), proto && proto.moveDirection ? proto.moveDirection : new Vec2(0,0), type == 'star' || type == 'planet', proto && proto.mass ? proto.mass : 0, this);
    this.position = this.pObj.position; //alias
    this.width = width;
    this.height = height;

    proto && proto.update && (this.update = proto.update);
    proto && proto.init && proto.init.call(this);


    if (this.type == 'star') {
        var config = clone(Game.particles['star']);
        config.pos.x = this.position.x;
        config.pos.y = this.position.y;
        var textures = [];
        config.texture.forEach(function(image){
            textures.push(PIXI.TextureCache[image])
        });
        var color;
        switch(myRandom(0,6)) {
            case 0:
                color = "#ffff00";
                break;
            case 1:
                color = "#00ff00";
                break;
            case 2:
                color = "#00ffff";
                break;
            case 3:
                color = "#ffffff";
                break;
            case 4:
                color = "#ffff00";
                break;
            case 5:
                color = "#ff0000";
                break;
        }
        config.color.start = color;
        config.color.stop = color;
        var emiter = new cloudkid.Emitter(
            Game.lvl.particlesContainer,
            textures,
            config
        );
        emiter.update(100); //preemit
        Game.lvl.particlesList.push(emiter);
    } else {
        //TESTED GRAPHICS, JUST COLORED CIRCLES;
        this.sprite = new PIXI.DisplayObjectContainer();
        this.cirlce = new PIXI.Graphics();
        this.cirlce.beginFill(proto && proto.color ? proto.color : 0xFFFFFF);
        this.cirlce.drawCircle(0, 0, this.width/2);
        this.cirlce.endFill();
        this.sprite.addChild(this.cirlce);
        this.sprite.position = this.position;
        Game.lvl.gameContainer.addChild(this.sprite);

        this.c = new PIXI.Graphics();
        this.c.beginFill(0xFFFFFF, 0.1);
        this.c.drawCircle(0, 0, this.width/2);
        this.c.endFill();
        this.c.position =  this.position;
        Game.lvl.tracerContainer.addChild(this.c);
    }
}

AstraObject.prototype.update = function () {
    if (this.type == 'star')
        return;

    Game.lvl.objectsList.forEach(function(obj) {
        if (obj != this && obj.pObj && obj.pObj.gravitySource) {
            this.pObj.applyAcceleration(obj.pObj);
        }
    }.bind(this))

    this.pObj.move();
    var collision = this.pObj.checkCollision(Game.lvl.objectsList);
    if (collision) {
        this.onCollision(collision);
    }

};


AstraObject.prototype.onCollision = function(obj) {
    console.log('collision');
};

AstraObject.prototype.generateSolarSystem = function (list) {
    var mass = 0;
    list.push(new AstraObject(0, 0, 'star', 64, 64, {mass:4096*4, color:0xFFFF00}));
    for (var i = 150; i< 700; i+=(mass=myRandom(60,120))){
        list.push(AstraObject.prototype.generatePlanet(i, mass));
    }
    //list.push(new AstraObject(-220, 0, 'planet', 24, 24, {mass:10, moveDirection:new Vec2(0, -4.5), color:0x0000FF}));
    //list.push(new AstraObject(-650, 0, 'planet', 36, 36, {mass:15, moveDirection:new Vec2(0, -3.5), color:0xF000FF}));
    //list.push(new AstraObject(-450, 0, 'planet', 36, 36, {mass:15, moveDirection:new Vec2(0, 4), color:0xF000FF}));
};

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

AstraObject.prototype.generatePlanet = function (distance, mass) {
    mass = mass/4;
    var position = new Vec2(0, 1);
    position.rotate(3.14*myRandom(0, 360)/180).multiply(distance);
    if (myRandom(0,10) == 0)
        distance *=0.7;
    //this.moveDirection.sub(vector.normalize().multiply(source.mass*gravityConstant/(distanceConstant*this.mass*length*length)));
    var moveDirection = position.clone().multiply(-1).normalize().rotate(3.14*(myRandom(0,2) == 1 ? 1 : -1)/2).normalize().multiply(Math.sqrt(2*25*0.025*4096*mass/distance));
    return new AstraObject(position.x, position.y, 'planet', mass, mass, {mass:mass/4, moveDirection:moveDirection, color:0xF000FF})
};
