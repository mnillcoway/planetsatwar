/**
 * Created by Mnill on 18.04.15.
 */

Game.particles = {
    blackhole: {
        texture: ["images/smoke.png"],
        "alpha": {
            "start": 1,
            "end": 0
        },
        "scale": {
            "start": 0.1,
            "end": 0.5,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#000000",
            "end": "#a3a3a3"
        },
        "speed": {
            "start": 12,
            "end": 13
        },
        "acceleration": {
            "x": 0,
            "y": 0
        },
        "startRotation": {
            "min": 0,
            "max": 360
        },
        "rotationSpeed": {
            "min": 0,
            "max": 0
        },
        "lifetime": {
            "min": 1,
            "max": 1
        },
        "blendMode": "normal",
        "frequency": 0.01,
        "emitterLifetime": -1,
        "maxParticles": 1000,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "point"
    },
    star: {
        texture: ["images/smoke.png"],
        "alpha": {
            "start": 0.54,
            "end": 0
        },
        "scale": {
            "start": 0.8,
            "end": 0.8,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#ffff00",
            "end": "#ffff00"
        },
        "speed": {
            "start": 1,
            "end": 3
        },
        "acceleration": {
            "x": 0,
            "y": 0
        },
        "startRotation": {
            "min": 0,
            "max": 360
        },
        "rotationSpeed": {
            "min": -4,
            "max": 4
        },
        "lifetime": {
            "min": 10,
            "max": 10
        },
        "blendMode": "add",
        "frequency": 0.2,
        "emitterLifetime": -1,
        "maxParticles": 1000,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "point"
    },
    rocket: {
        texture: ["images/smoke.png"],
        "alpha": {
            "start": 0.5,
            "end": 0.04
        },
        "scale": {
            "start": 0.05,
            "end": 0.1,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#00ffff",
            "end": "#00ffff"
        },
        "speed": {
            "start": 0,
            "end": 0
        },
        "acceleration": {
            "x": 0,
            "y": 0
        },
        "startRotation": {
            "min": -10,
            "max": 10
        },
        "rotationSpeed": {
            "min": 0,
            "max": 2
        },
        "lifetime": {
            "min": 0.5,
            "max": 2
        },
        "blendMode": "normal",
        "frequency": 0.05,
        "emitterLifetime": -1,
        "maxParticles": 1000,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "point"
    },
    rocketExplosion: {
        texture: ["images/smoke.png"],
        "alpha": {
            "start": 1,
            "end": 0
        },
        "scale": {
            "start": 0.1,
            "end": 0.5,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#fc0505",
            "end": "#e4f525"
        },
        "speed": {
            "start": 10,
            "end": 20
        },
        "acceleration": {
            "x": 0,
            "y": 0
        },
        "startRotation": {
            "min": -360,
            "max": 360
        },
        "rotationSpeed": {
            "min": 0,
            "max": 2
        },
        "lifetime": {
            "min": 0.5,
            "max": 2
        },
        "blendMode": "screen",
        "frequency": 0.001,
        "emitterLifetime": 0.02,
        "maxParticles": 1000,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "point"
    },
    planetCrash1 :{
        "alpha": {
            "start": 1,
            "end": 0
        },
        "scale": {
            "start": 0.2,
            "end": 0.1,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#fc0505",
            "end": "#e1f503"
        },
        "speed": {
            "start": 30,
            "end": 10
        },
        "acceleration": {
            "x": 0,
            "y": 1
        },
        "startRotation": {
            "min": 0,
            "max": 360
        },
        "rotationSpeed": {
            "min": 0,
            "max": 360
        },
        "lifetime": {
            "min": 0.5,
            "max": 2
        },
        "blendMode": "add",
        "frequency": 0.5,
        "emitterLifetime": 1,
        "maxParticles": 1000,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "burst",
        "particlesPerWave": 120,
        "particleSpacing": 3,
        "angleStart": 50
    },
    background: {
        texture: ["images/star1.png"],
        "alpha": {
            "start": 0.25,
            "end": 0.10
        },
        "scale": {
            "start": 0.1,
            "end": 0.1,
            "minimumScaleMultiplier": 1
        },
        "color": {
            "start": "#ffffff",
            "end": "#ffffff"
        },
        "speed": {
            "start": 0,
            "end": 0
        },
        "acceleration": {
            "x": 0,
            "y": 0
        },
        "startRotation": {
            "min": 0,
            "max": 0
        },
        "rotationSpeed": {
            "min": 0,
            "max": 0
        },
        "lifetime": {
            "min": 20,
            "max": 20
        },
        "blendMode": "normal",
        "frequency": 0.1,
        "emitterLifetime": -1,
        "maxParticles": 200,
        "pos": {
            "x": 0,
            "y": 0
        },
        "addAtBack": false,
        "spawnType": "rect",
        "spawnRect": {
            "x": -1024,
            "y": -768,
            "w": 2048,
            "h": 1536
        }
    }
}