/**
 * Created by Mnill on 18.04.15.
 */

// my default vectors library

function Vec2(x, y) {
    this.x = x;
    this.y = y;
}

// Get rotation from move vectors!
Vec2.prototype.getRotate = function (){
    var rotate = Math.acos((0*this.x +1*this.y)/(Math.sqrt((Math.pow(0,2)+Math.pow(1,2))*(Math.pow(this.x,2)+Math.pow(this.y,2)))));
    if (this.x < 0)
        rotate = Math.PI*2-rotate;
    if (isNaN(rotate))
        return 0;
    return rotate;
};

Vec2.prototype.getRotated = function (angle){
    return this.clone().rotate(-angle);
};

Vec2.prototype.rotate = function (angle){
    var cachePoint = new Vec2(this.x, this.y);
    this.x  = cachePoint.x * Math.cos(angle) - cachePoint.y * Math.sin(angle);
    this.y  = cachePoint.x * Math.sin(angle) + cachePoint.y * Math.cos(angle);
    return this;
};

Vec2.prototype.multiply = function (factor){
    this.x*=factor;
    this.y*=factor;
    return this;
};

Vec2.prototype.multiplyed = function (factor){
    return new Vec2(this.x*factor, this.y*factor);
};

Vec2.prototype.add = function (vecToAdd) {
    if (!(vecToAdd instanceof Vec2)&&(arguments.length == 2))
        vecToAdd = new Vec2(arguments[0], arguments[1]);
    this.x+=vecToAdd.x;
    this.y+=vecToAdd.y;
    return this;
};

Vec2.prototype.sub = function (vecToSub){
    this.x-=vecToSub.x;
    this.y-=vecToSub.y;
    return this;
};


Vec2.prototype.clone = function () {
    return new Vec2(this.x, this.y);
};

Vec2.prototype.set = function (x, y) {
    if (x instanceof Vec2){
        this.x = x.x;
        this.y = x.y;
    } else {
        this.x = x;
        this.y = y;
    }
};

Vec2.prototype.normalize = function (){
    var length = this.getLength();
    if (length){
        this.multiply(1/this.getLength());
    }
    return this;
};

Vec2.prototype.getLength = function (){
    return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2));
};

Vec2.prototype.getDirection = function (){
    if (this.x > 0)
        return 1;
    else
        return -1;
};

//for now emulate only circles physics objects
//all units of measurement in seconds or per second.
function PObject(x, y, width, height, direction, moveDirection, gravitySource, mass, parent) {
    this.parent = parent; //parent object
    this.width = width;
    this.height = height;
    this.pCircleRadius = Math.max(width, height)/2; //circle radius for collision
    this.position = new Vec2(x,y); //position
    this.gravitySource = gravitySource;
    this.direction = direction; //direction of object for future mb truster of rockets, and self sourced acceleration Not used now; Vec2
    this.moveDirection = moveDirection;  //acceleration vector
    this.mass = mass; //just mass
}

//question - first move or first gravitation acceleration apply
PObject.prototype.move = function() {
    this.position.add(this.moveDirection.multiplyed(Game.cycleTickTime/1000));
};

PObject.prototype.applyAcceleration = function (source) {
    var gravityConstant = 0.0005*50;
    var distanceConstant = 4;

    var vector = this.position.clone().sub(source.position);
    var length = vector.getLength();
    if (length <=1)
        length = 1;

    //if (this.parent.type == 'sputnik')
    //    distanceConstant = 0.1;

    var gravitation = (gravityConstant * source.mass * this.mass) / (length * length);
    //if (gravitation > 30)
    //    gravitation = 30;

    this.moveDirection.sub(vector.normalize().multiply(gravitation));
};


PObject.prototype.checkCollision = function(mask) {
    for (var i = 0; i< mask.length; i++)
    {
        if ((Math.abs(mask[i].pObj.position.x - this.position.x)< Math.max(this.pCircleRadius , mask[i].pObj.pCircleRadius))&&(Math.abs(mask[i].pObj.position.y - this.position.y)< Math.max(this.pCircleRadius, mask[i].pObj.pCircleRadius)))
        {
            if(mask[i] != this.parent)
                return mask[i];
        }
    }
    return false;
};


